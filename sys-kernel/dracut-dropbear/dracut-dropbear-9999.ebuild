# $Header: $

EAPI="5"
SLOT="0"

inherit git-2

DESCRIPTION="Module dropbear for dracut."

HOMEPAGE="http://chaserpro.net"

LICENSE="GPLv3+"

KEYWORDS="~amd64 ~x86"

EGIT_BRANCH="master"
EGIT_REPO_URI="http://git.geek.km.ua/chaser/dropbear-dracut-module.git"

RDEPEND="
	net-misc/dropbear
	sys-kernel/dracut"

src_compile() {
    $(tc-getCC) ${LDFLAGS} ${CFLAGS} -std=c99 auth.c -o auth
}

src_install() {
    dodir /usr/lib/dracut/modules.d/91dropbear
    cp -R "${S}"/* "${ED}"/usr/lib/dracut/modules.d/91dropbear
    chmod +x "${ED}"/usr/lib/dracut/modules.d/91dropbear/create_dropbear_dsa_key.sh
#    insinto /usr/lib/dracut/modules.d/90dropbear
#    exeinto /usr/lib/dracut/modules.d/90dropbear
#    doins module-setup.sh            || die
#    doexe create_dropbear_dsa_key.sh || die
#    doins README                     || die
#    doins dropbear-start.sh          || die
#    doins etc/dropbear/.keep         || die
#    doins etc/shells                 || die
#    doins etc/passwd                 || die
#    doins etc/dropbear/.keep         || die
#    doins auth                       || die
#    doins root/.ssh/.keep            || die

}
