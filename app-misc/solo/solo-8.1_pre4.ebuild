# $Header: $

EAPI="5"
SLOT="0"

inherit unpacker

DESCRIPTION="Typing tutor."

HOMEPAGE="http://ergosolo.ru"

BASE_URI="http://ergosolo.ru/download/linux/pre4"
SRC_URI="${BASE_URI}/x86_64/Debian_Lenny/solo8_8.X.pre4-8_amd64.deb
         ${BASE_URI}/noarch/DEBS/solo8-data-animations_8.X.pre4-1_all.deb
         http://archive.debian.org/debian/pool/main/libg/libgtkhtml2/libgtkhtml2-0_2.11.1-2_amd64.deb
         course-russian? ( ${BASE_URI}/noarch/DEBS/solo8-data-packages-russian_8.X.pre4-1_all.deb )
         "

#LICENSE="EULA"

KEYWORDS="~amd64"

IUSE="course-russian crack"

S=${WORKDIR}


RDEPEND="
        crack? ( >=app-editors/vim-7.3.762 )
        "
# 32-bit
# echo "3ED0E: 31 C0 40" | xxd -r - solo8
# xor %eax, %eax
# inc %eax


# 64-bit
# echo "3E46C: 6A 01 58" | xxd -r - solo8
# push $1
# pop %rax



src_compile()
{
    if use crack; then
        # patch binary file
        if use x86; then
            echo "3ED0E: 31 C0 40" | xxd -r - ${WORKDIR}/usr/bin/solo8;
        fi

        if use amd64; then
            echo "3E46C: 6A 01 58" | xxd -r - ${WORKDIR}/usr/bin/solo8;
        fi

    fi
}

src_install()
{
#    dodir /usr/lib/dracut/modules.d/91dropbear
     cp -R ${WORKDIR}/* "${ED}"
     rm -r ${ED}/usr/share/gnome


#    chmod +x "${ED}"/usr/lib/dracut/modules.d/91dropbear/create_dropbear_dsa_key.sh
#    insinto /usr/lib/dracut/modules.d/90dropbear
#    exeinto /usr/lib/dracut/modules.d/90dropbear
#    doins module-setup.sh            || die
#    doexe create_dropbear_dsa_key.sh || die
#    doins README                     || die
#    doins dropbear-start.sh          || die
#    doins etc/dropbear/.keep         || die
#    doins etc/shells                 || die
#    doins etc/passwd                 || die
#    doins etc/dropbear/.keep         || die
#    doins auth                       || die
#    doins root/.ssh/.keep            || die

}
