# Copyright 2014 CarelessChaser
# Distributed under the terms of the GNU General Public License v3 or higher
# $HEADER: $

EAPI=5

inherit eutils systemd versionator user java-pkg-2

MAJOR_PV="$(get_version_component_range 1-2)"
REL_PV="$(get_version_component_range 3)"
SVN_PV="$(get_version_component_range 4)"

DESCRIPTION="YaCy - p2p based distributed web-search engine"
HOMEPAGE="http://www.yacy.net/"
SRC_URI="http://www.yacy.net/release/yacy_v${MAJOR_PV}_${REL_PV}_${SVN_PV}.tar.gz"
SLOT="0"
KEYWORDS="~x86 ~amd64"
DEPEND=">=virtual/jdk-1.6"
LICENSE="GPL-2"

S="${WORKDIR}/yacy"
#EANT_BUILD_TARGET=all



src_prepare() {
#	rm -r "${S}"/lib/*.jar || die
#}

	# remove win-only stuff
	find "${S}" -name "*.bat" -exec rm '{}' \; || die
    rm -r "${S}"/addon/Notepad++ || die
    rm -r "${S}"/addon/windowsService || die
    # remove macos-only stuff
    rm -r "${S}"/addon/YaCy.app || die
	# remove sources
	rm -r "${S}"/source || die
	rm "${S}/build.properties" "${S}/build.xml" || die

	# remove init-scripts
	rm "${S}"/*.sh || die
}

src_install() {
	dodoc AUTHORS COPYRIGHT NOTICE gpl.txt readme.txt
	dodoc lib/*License

	yacy_home="${EROOT}usr/share/${PN}"
	dodir ${yacy_home}
	cp -r "${S}"/* "${D}${yacy_home}" || die

	rm "${D}${yacy_home}"/{AUTHORS,COPYRIGHT,NOTICE,gpl.txt,readme.txt}
	rm -r "${D}${yacy_home}"/lib/*License

	dodir /var/log/yacy
	fowners yacy:yacy /var/log/yacy

	dosym /var/lib/yacy /${yacy_home}/DATA

	systemd_newunit "${FILESDIR}"/${PN}.service ${PN}.service
	newconfd "${FILESDIR}"/${PN}.confd ${PN}
}


pkg_setup() {
	enewgroup yacy
	enewuser yacy -1 -1 /var/lib/yacy yacy
}


pkg_postinst() {
	einfo "yacy.logging will write logfiles into /var/lib/yacy/LOG"
	einfo "To setup YaCy, open http://localhost:8090 in your browser."
}
