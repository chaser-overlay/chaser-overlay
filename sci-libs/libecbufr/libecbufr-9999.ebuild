# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit bzr autotools

DESCRIPTION="libECBUFR is a general purpose, template-oriented BUFR encoding/decoding library."
HOMEPAGE="https://launchpad.net/libecbufr"
EBZR_REPO_URI="lp:libecbufr"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=( app-docs/doxygen )


src_prepare() {
    ${S}/reconf || die
}

#src_configure() {
 #   local myeconfargs=(
 #       $(use_enable foo)
 #   )
#    autotools-utils_src_configure
#}

#src_compile() {
#    econf $(use_enable nls)
#    emake
#}
